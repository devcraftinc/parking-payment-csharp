﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;

namespace ParkingPayment
{
  public class DayOfWeekPricing : Pricing
  {
    readonly TimeSpan morningThreshold;
    readonly Dictionary<DayOfWeek, Pricing> perDay;

    public DayOfWeekPricing(Dictionary<DayOfWeek, Pricing> perDay, TimeSpan morningThreshold)
    {
      this.perDay = perDay;
      this.morningThreshold = morningThreshold;
    }

    public int CalculatePrice(Interval interval)
    {
      var morning = interval.EntryTime.Date.Add(morningThreshold);
      if (morning > interval.EntryTime)
        morning = morning.AddDays(-1);

      var price = 0;

      do
      {
        var pricing = perDay[morning.DayOfWeek];
        morning = morning.AddDays(1);
        var timeToday = interval.EndBefore(morning);
        price += pricing.CalculatePrice(timeToday);
        interval = interval.StartingAt(morning);
      } while (interval.Exists());

      return price;
    }
  }
}