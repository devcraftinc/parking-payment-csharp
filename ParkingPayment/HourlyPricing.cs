﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace ParkingPayment
{
  public class HourlyPricing : Pricing
  {
    readonly TimeSpan incrementLength;
    readonly int incrementPrice;
    readonly TimeSpan initialBlockLength;
    readonly int initialBlockPrice;

    public HourlyPricing(TimeSpan initialBlockLength, int initialBlockPrice, TimeSpan incrementLength, int incrementPrice)
    {
      this.initialBlockLength = initialBlockLength;
      this.initialBlockPrice = initialBlockPrice;
      this.incrementLength = incrementLength;
      this.incrementPrice = incrementPrice;
    }

    public int CalculatePrice(Interval interval)
    {
      if (interval.Length() < initialBlockLength)
        return initialBlockPrice;

      var increments = (int) (1 + (interval.Length() - initialBlockLength).Ticks / incrementLength.Ticks);

      return initialBlockPrice + increments * incrementPrice;
    }
  }
}